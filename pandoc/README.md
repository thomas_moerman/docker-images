# Converting markdown/latex to html/pdf

```
docker run -v `pwd`:/source jandot/pandoc -f markdown -t latex /source/myfile.md -o /source/myfile.pdf
```
