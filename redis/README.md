# Mongo docker container
## Building and running
```
sh docker-build.sh
sh docker-run.sh
```

### Issues
The data is not persisted yet outside of the container itself.

## Testing
If the container is running, check if you can get in with ssh: `ssh -p 30022 root@192.168.99.100`.

To check redis, run another *interactive* redis container with `docker run --rm -it jandot/redis /bin/bash`. When in that container, try `redis-cli -h 192.168.99.100 ping`, which should return `PONG`.
